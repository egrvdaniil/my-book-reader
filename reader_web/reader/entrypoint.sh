#!/bin/sh

set -e

. /venv/bin/activate

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi
python manage.py migrate
python manage.py prepopulate_books
python manage.py collectstatic --no-input --clear

if [ "$DEBUG" = "0" ]
then
  exec gunicorn reader.wsgi:application --bind 0.0.0.0:8000
else
  exec python manage.py runserver 0.0.0.0:8000
fi
