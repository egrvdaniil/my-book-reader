from typing import List
from .types import Chapter, TableOfContent


def _create_chapter(chapter: List) -> Chapter:
    return Chapter(
        level=chapter[0],
        title=chapter[1],
        page=chapter[2],
        inner_chapters=[],
    )


def _add_chapter(ch_list: List[Chapter], gen) -> List:
    this_level = ch_list[-1].level
    while True:
        chapter = next(gen)
        if chapter[0] == this_level:
            ch_list.append(_create_chapter(chapter))
            continue
        if chapter[0] > this_level:
            ch_list[-1].inner_chapters.append(_create_chapter(chapter))
            low_lvl_ch = _add_chapter(ch_list[-1].inner_chapters, gen)
            if this_level == low_lvl_ch[0]:
                ch_list.append(_create_chapter(low_lvl_ch))
                continue
            if low_lvl_ch[0] < this_level:
                return low_lvl_ch
        if chapter[0] < this_level:
            return chapter


def format_toc(toc: List) -> TableOfContent:
    """
    Переводит список глав из PyMuPDF в рекурсивную структуру.
    """
    out_list: List[Chapter] = list()
    out_list.append(_create_chapter(toc[0]))
    try:
        _add_chapter(out_list, (ch for ch in toc[1:]))
    except StopIteration:
        pass
    return TableOfContent(chapters=out_list)
