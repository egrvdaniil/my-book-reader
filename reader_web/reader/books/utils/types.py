from typing import List, Union
from dataclasses import dataclass
from typing import Union


@dataclass
class Chapter:
    level: int
    title: str
    page: int
    inner_chapters: Union[List, None]


@dataclass
class TableOfContent:
    chapters: List[Chapter]
