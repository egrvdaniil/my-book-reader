from ..models import Book, Render
import fitz
from django.contrib.auth.models import User as DjangoUser
from typing import List
from ..crud import read, transactions


def render_page(
    book: Book,
    user: DjangoUser,
    page_num: int,
    render_type: str,
) -> Render:
    """
    Рендерит страницу с указанным номером и обновляет количество рендеров у указанного пользователя
    """
    document = fitz.Document(book.file_field.path, filetype=book.file_field.name)
    page = document.load_page(page_num - 1)
    rendered_page = page.get_svg_image()
    render = transactions.create_new_render(
        book, user, page_num, rendered_page, render_type
    )
    document.close()
    return render


def get_or_create_one_page(book: Book, user, page_num, render_type="svg"):
    try:
        page = Render.objects.get(book=book, page_num=page_num)
    except Render.DoesNotExist:
        page = render_page(book, user, page_num, render_type)
    return page


def render_pages_list(
    book: Book,
    user: DjangoUser,
    render_type: str,
    pages_list: List[int],
) -> List[Render]:
    """
    Возвращает список отрендеренных страниц с номерами указанными в pages_list
    Рендерит страницы если они не отрендеренны.
    """
    renders = list(read.get_renders_by_list(book, pages_list))
    renders_out = []
    i = 0
    for page in pages_list:
        try:
            if renders[i].page_num == page:
                renders_out.append(renders[i])
                i += 1
            else:
                renders_out.append(render_page(book, user, page, render_type))
        except IndexError:
            renders_out.append(render_page(book, user, page, render_type))
    return renders_out


def get_or_create_book_pages(
    book: Book,
    user: DjangoUser,
    page_num: int,
    buf_size: int = 3,
    render_type: str = "svg",
) -> List[Render]:
    """
    Возвращает список отрендеренных страниц с номерами от page_num-buf_size до page_num+buf_size
    """
    pages_start = 1 if (dif := page_num - buf_size) < 1 else dif
    pages_end = book.pages if (dif := page_num + buf_size) > book.pages else dif
    load_pages = list(range(pages_start, pages_end + 1))
    renders = render_pages_list(book, user, render_type, load_pages)
    return renders
