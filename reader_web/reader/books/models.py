from django.db import models
from django.contrib import auth
from sorl.thumbnail import ImageField
import os.path


class Book(models.Model):
    book_name = models.CharField(max_length=200, help_text="Name of the book")
    file_field = models.FileField(
        upload_to="books/", help_text="Name of loaded book file"
    )
    thumbnail = ImageField(
        upload_to="book_thumbnails/", null=True, help_text="Thumbnail of the book"
    )
    user = models.ForeignKey(auth.get_user_model(), on_delete=models.CASCADE)
    pages = models.IntegerField(help_text="Number of pages in the book")


class Progress(models.Model):
    user = models.ForeignKey(auth.get_user_model(), on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    progress = models.IntegerField(help_text="Page, user is stopped on.")
    last_read = models.DateTimeField(
        help_text="Время последнего прочтения", auto_now=True, null=True
    )


def get_render_file_path(instance, filename):
    return os.path.join("book_render", str(instance.book.id), filename)


class Render(models.Model):
    class RenderTypes(models.TextChoices):
        IMAGE = "png"
        SVG = "svg"
        HTML = "html"

    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    render_file = models.FileField(upload_to=get_render_file_path)
    page_num = models.IntegerField(help_text="Page number of book")
    render_type = models.CharField(choices=RenderTypes.choices, max_length=10)
    date_rendered = models.DateTimeField(auto_now_add=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["book", "page_num", "render_type"], name="unique_render"
            ),
        ]


class UserRender(models.Model):
    render = models.ForeignKey(Render, on_delete=models.CASCADE)
    user = models.ForeignKey(auth.get_user_model(), on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["render", "user"], name="unique_user-render"
            ),
        ]
