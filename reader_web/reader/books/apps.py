from django.apps import AppConfig


class BooksConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'books'

    def _ready_login_require(self):
        from django.contrib.auth import views

        login_not_required_views = (
            views.LoginView,
            views.PasswordResetView,
            views.PasswordResetDoneView,
            views.PasswordResetConfirmView,
            views.PasswordResetCompleteView,
        )
        for view in login_not_required_views:
            view.login_required = False

    def ready(self):
        self._ready_login_require()
