from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
import os


class Command(BaseCommand):
    def handle(self, *args, **options):
        User = get_user_model()
        if User.objects.count() == 0:
            username = os.environ.get("SUPERUSER_NAME", default="admin")
            print(f"Creating user {username}")
            email = os.environ.get("SUPERUSER_EMAIL", default="admin@books.com")
            password = os.environ.get("SUPERUSER_PASSWORD", default="admin")
            User.objects.create_superuser(username, email, password)
            print("User created")
            return
        print("User already exist.")

