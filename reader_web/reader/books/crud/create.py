from ..models import Render, Book, UserRender
from django.core.files.base import ContentFile
from django.db import IntegrityError
from django.contrib.auth.models import User as DjangoUser


def new_render(
    book: Book, page_num: int, render_type: str, rendered_page: str
) -> Render:
    """
    Создает новый рендер,
    Лучше использовать create_new_render из transactions
    """
    render = Render(book=book, page_num=page_num, render_type=render_type)
    render.render_file.save(f"{page_num}.{render_type}", ContentFile(rendered_page))
    return render


def create_user_render(user: DjangoUser, render: Render) -> UserRender:
    user_render = UserRender.objects.create(render=render, user=user)
    return user_render
