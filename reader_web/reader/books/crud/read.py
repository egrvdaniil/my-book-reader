from typing import List, Union
from ..models import Render, Book, Progress
from collections.abc import Iterable
from django.db.models import QuerySet
from django.contrib.auth.models import User as DjangoUser


def get_renders_by_list(book, pages_list: List[int]) -> QuerySet[Render]:
    return Render.objects.filter(book=book, page_num__in=pages_list).order_by(
        "page_num"
    )


def get_last_page(book: Book, user):
    try:
        progress = Progress.objects.get(book=book, user=user).progress
    except Progress.DoesNotExist:
        progress = 1
    return progress


def get_book_progress_info(
    books: Union[Iterable, QuerySet], user: DjangoUser
) -> List[Progress]:
    """
    Возвращает прогресс каждой книги из списка указанного пользователя в виде списка Progress
    """
    out_progress = []
    for book in books:
        try:
            progress = Progress.objects.get(book=book, user=user)
        except Progress.DoesNotExist:
            progress = Progress(
                user=user,
                book=book,
                progress=1,
                last_read=None,
            )
        out_progress.append(progress)
    return out_progress


def get_sorted_books(
    sort_by: str,
    order_by: str,
) -> QuerySet:
    """
    Возвращает список кник с указанной сортировкой
    """
    if order_by == "desc":
        order = "-"
    else:
        order = ""
    if sort_by == "name":
        books = Book.objects.order_by(order + "book_name").all()
    else:
        books = Book.objects.order_by(order + "id").all()
    return books
