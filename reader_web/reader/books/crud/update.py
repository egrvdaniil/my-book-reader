from ..models import Book, Progress


def update_progress(book: Book, user, page_num):
    try:
        progress = Progress.objects.get(book=book, user=user)
    except Progress.DoesNotExist:
        progress = Progress(book=book, user=user)
    progress.progress = page_num
    progress.save()
