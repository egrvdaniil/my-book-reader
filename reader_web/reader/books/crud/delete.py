from ..models import UserRender


def _delete_render_if_no_users(user_render: UserRender):
    """
    Удаляет отрендеренную страницу пользоватеся если он единственный владелец
    """
    render = user_render.render
    if render.userrender_set.count() == 1:
        render.delete()
        return
    user_render.delete()


def delete_excess_renders(user, max_pages: int = 500) -> None:
    """
    Проверяет сколько количество отрендереных страниц, если их количество больше max_pages
    удаляет самые старые пока их колличество не будет равно max_pages
    """
    renders = UserRender.objects.filter(user=user).count()
    if renders <= max_pages:
        return
    user_renders = (
        UserRender.objects.filter(user=user).order_by("-date").all()[max_pages:]
    )
    for user_render in user_renders:
        _delete_render_if_no_users(user_render)
