from ..models import Book, Render
from django.contrib.auth.models import User as DjangoUser
from . import delete, create
from django.db import transaction as tr


def create_new_render(
    book: Book,
    user: DjangoUser,
    page_num: int,
    rendered_page: str,
    render_type="svg",
    max_pages=500,
) -> Render:
    """
    Создает новый рендер, и удаляет старый если общее их количество больше определенного числа
    """
    with tr.atomic():
        render = create.new_render(book, page_num, render_type, rendered_page)
        create.create_user_render(user, render)
        delete.delete_excess_renders(user, max_pages=max_pages)
    return render
