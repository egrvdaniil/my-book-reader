from django import forms
from .models import Book

class UploadBook(forms.Form):
    file = forms.FileField()


class BookEdit(forms.ModelForm):
    class Meta:
        model = Book
        fields = ("book_name", )