from django.contrib.auth.middleware import AuthenticationMiddleware
from django.contrib.auth.views import redirect_to_login
from django.http import Http404
from django.urls import resolve


def login_not_required(view_func):
    view_func.login_required = False
    return view_func


class LoginNotRequiredMixin:
    login_required = False


class LoginRequiredMiddleware(AuthenticationMiddleware):
    def process_request(self, request):
        if request.user.is_authenticated:
            return None
        path = request.path
        try:
            resolver = resolve(path)
        except Http404:
            return redirect_to_login(path)
        view_func = resolver.func
        if not getattr(view_func, "login_required", True):
            return None
        view_class = getattr(view_func, "view_class", None)
        if view_class and not getattr(view_class, "login_required", True):
            return None
        return redirect_to_login(path)
