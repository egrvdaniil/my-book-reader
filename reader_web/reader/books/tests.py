from django.test import TestCase
from unittest import TestCase
from .utils.toc import format_toc


class TestUtils(TestCase):
    def test_format_toc(self):
        test_data = [
            [1, "Title1", 1],
            [2, "Title2", 2],
            [3, "Title3", 3],
            [1, "Title4", 4],
            [2, "Title5", 5],
            [3, "Title6", 6],
            [3, "Title7", 7],
            [2, "Title8", 8],
            [3, "Title9", 9],
            [4, "Title10", 10],
            [5, "Title11", 11],
            [1, "Title12", 12],
        ]

        formated_toc = format_toc(test_data)
        self.assertEqual(len(formated_toc.chapters), 3)
        self.assertEqual(len(formated_toc.chapters[1].inner_chapters), 2)
        self.assertEqual(
            formated_toc.chapters[1].inner_chapters[1].inner_chapters[0].title, "Title9"
        )
