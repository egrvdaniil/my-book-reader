from django.shortcuts import render, redirect, get_object_or_404
from django.http import (
    HttpRequest,
    HttpResponseForbidden,
    HttpResponseNotFound,
    HttpResponse,
)
from django.core.files.uploadedfile import UploadedFile
from django.core.files.base import ContentFile
from django.utils.text import slugify
from .models import Book
from .forms import UploadBook, BookEdit
import fitz
from django.core.paginator import Paginator
from .utils import render as book_render, toc
from .crud import update, read


# Create your views here.
def index(request: HttpRequest):
    return render(request, "books/index.html")


def load_book(request: HttpRequest):
    if request.method == "POST":
        form = UploadBook(request.POST, request.FILES)
        if form.is_valid():
            upload: UploadedFile = request.FILES["file"]
            uploaded_book = fitz.Document(stream=upload.read(), filetype=upload.name)

            book_name = uploaded_book.metadata["title"]
            book = Book(
                book_name=book_name,
                file_field=upload,
                user=request.user,
                pages=uploaded_book.page_count,
                progress=0,
            )
            first_page = uploaded_book.load_page(0)
            thumbnail = ContentFile(first_page.get_pixmap().tobytes())
            book.thumbnail.save(f"{slugify(book_name)}.png", thumbnail)
            return redirect("book_detail", book.pk)
    form = UploadBook()
    return render(request, "books/book_load_form.html", {"form": form})


def book_detail(request: HttpRequest, book_id: int):
    book = get_object_or_404(Book, id=book_id)
    progress = read.get_last_page(book, request.user)
    owner = request.user == book.user
    if request.method == "GET":
        action = request.GET.get("action", "")
        if action == "":
            return render(request, "books/book_detail.html", {"book": book})
        if action == "info":
            context = {"book": book, "owner": owner, "progress": progress}
            return render(request, "books/ajax/book_detail/info.html", context)
        if action == "edit-form":
            if not owner:
                return HttpResponseForbidden()
            edit_form = BookEdit(instance=book)
            return render(
                request, "books/ajax/book_detail/form.html", {"edit_form": edit_form}
            )
    if request.method == "POST":
        if not owner:
            return HttpResponseForbidden()
        if request.method == "POST":
            edit_form = BookEdit(request.POST, instance=book)
            if edit_form.is_valid():
                edit_form.save()
                context = {"book": book, "owner": owner, "progress": progress}
                return render(request, "books/ajax/book_detail/info.html", context)
    return HttpResponseForbidden()


def book_read(request: HttpRequest, book_id: int):
    book = get_object_or_404(Book, id=book_id)
    action = request.GET.get("action", "")
    if action == "":
        last_page = read.get_last_page(book, request.user)
        renders = book_render.get_or_create_book_pages(book, request.user, last_page)
        context = {
            "book_id": book.id,
            "renders": renders,
            "page_num": last_page,
            "max_pages": book.pages,
        }
        return render(request, "books/reader.html", context)
    if action == "get-page":
        page_num = int(request.GET.get("page-num", ""))
        if update_page := request.GET.get("update-page", None):
            update.update_progress(book, request.user, update_page)
        if page_num < 1 or page_num > book.pages:
            return HttpResponseNotFound()
        page = book_render.get_or_create_one_page(book, request.user, page_num)
        return render(request, "books/ajax/reader/rendered_page.html", {"page": page})
    if action == "update-page":
        page_num = int(request.GET.get("page-num", ""))
        update.update_progress(book, request.user, page_num)
        return HttpResponse()
    if action == "go-to-page":
        page_num = int(request.GET.get("page-num", ""))
        update.update_progress(book, request.user, page_num)
        return redirect("book_read", book.id)
    return render(request, "books/reader.html")


def book_list(request: HttpRequest):
    sort_by = request.GET.get("sort_by", "")
    order_by = request.GET.get("order_by", "")
    books = read.get_sorted_books(sort_by, order_by)
    page = request.GET.get("page", "")
    page_num = 1
    try:
        page_num = int(page)
    except ValueError:
        pass
    paginator = Paginator(books, 9)
    books_page = paginator.page(page_num)
    page_count = paginator.num_pages
    progresses = read.get_book_progress_info(books_page.object_list, request.user)
    return render(
        request,
        "books/book_list.html",
        {
            "books": progresses,
            "page_num": page_num,
            "page_count": page_count,
            "sort_by": sort_by,
            "order_by": order_by,
        },
    )


def get_book_toc(request: HttpRequest, book_id: int):
    book = get_object_or_404(Book, id=book_id)
    document = fitz.Document(book.file_field.path, filetype=book.file_field.name)
    book_toc = document.get_toc()
    formated_toc = toc.format_toc(book_toc)
    return render(
        request,
        "books/ajax/book_detail/toc.html",
        {"table_of_content": formated_toc, "book_id": book.id},
    )


def delete_book(request: HttpRequest, book_id):
    book = get_object_or_404(Book, id=book_id)
    book.delete()
    return redirect("book_list")


def search_book(request: HttpRequest):
    search_query = request.GET.get("q", "")
    if not search_query:
        return HttpResponseForbidden("Пустой поисковый запрос")
    # Заменить на более сложный поиск
    found_books = Book.objects.filter(book_name__icontains=search_query).all()[:10]
    progresses = read.get_book_progress_info(found_books, request.user)
    return render(
        request,
        "books/search_page.html",
        {"found_books": progresses, "query": search_query},
    )
