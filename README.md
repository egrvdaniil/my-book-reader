# Онлайн читалка
Моя читалка для книг и документов в формате PDF и EPUB.
![Пример работы](https://s7.gifyu.com/images/ezgif.com-gif-makerd998d3f93b67a4c7.gif)
## Установка
1. Установите Docker [инструкция](https://docs.docker.com/engine/install/).
2. Установите Docker Compose [инструкция](https://docs.docker.com/compose/install/).
3. Переименуйте файл `template_env` на `.env`.
4. Измените в файле .env нужные параметры.
   * `DEBUG` 1 для включения дебаг мода.
   * `SECRET_KEY` секретный ключ django. Можно сгенерировать например [здесь](https://djecrety.ir).
   * `DJANGO_ALLOWED_HOSTS` список разрешенных хостов, через побел например `localhost` `127.0.0.1` `192.168.0.1` `myhost`
   * `DATABASE` название базы данных
   * `SQL_ENGINE` движок базы данных
   * `SQL_DATABASE` название базы данных
   * `SQL_USER` имя пользователя для базы данных
   * `SQL_PASSWORD` пароль для базы данных
   * `SQL_HOST` имя хоста базы данных 
   * `SQL_PORT` порт базы данных 
   * `SUPERUSER_NAME` имя суперпользователя приложения
   * `SUPERUSER_EMAIL` email адрес суперпользователя приложения
   * `SUPERUSER_PASSWORD` пароль суперпользователя приложения
   * `APP_PORT` порт приложения
5. Запустите приложение командой `docker-compose up -d` 

Логин и пароль от приложения указываются в .env файле. Стандартные значения `admin`, `admin`.
